class IVAOWeatherProvider extends WeatherProvider

	constructor: ->
		super

	fetchAllWeather: (callback) ->
		@retrieved = new Date()
		@metars = []

		provider = @

		req = $.ajax 
			#url: 'http://eu20.ivan.ivao.aero/whazzup.txt'
			url: 'http://localhost:8080/weather/IVAO'
			datatype: 'jsonp'
			success: (data) ->
				provider.parseData data	
				do callback
			error: ->
				console.log 'Error'

	parseData: (data) ->
		start = new Date()
		lines = data.split "\n"
		for line in lines
			items = line.split " "
			@metars.push new Metar "rawMetar": line
			
		finish = new Date()
		console.log finish - start 

window.IVAOWeatherProvider = IVAOWeatherProvider
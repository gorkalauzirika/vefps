class FlightProvider

	constructor: ->
		@retrieved = null
		@flights = []

	#Returns an array of Flights matching the string given
	searchByFlightNumber: (flightNumber) ->
		for flight in @flights
			if flight.attributes.flightNumber is flightNumber
				return flight
		return null

	filterByFlightNumber: (flightNumber) ->
		_.filter @flights, (flight) ->
			flight.attributes.flightNumber.indexOf(flightNumber) != -1

	filterByAirports: (airports) ->
		filtered = {}
		filtered.outbound = _.filter @flights, (flight) ->
			$.inArray(flight.get("departure"), airports) > -1
		filtered.inbound = _.filter @flights, (flight) ->
			$.inArray(flight.get("arrival"), airports) > -1
		filtered

	fetchAllFlights : (callback) ->
		@retrieved = new Date()
		do callback

window.FlightProvider = FlightProvider
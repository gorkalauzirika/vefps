class FooFlightProvider extends FlightProvider

	constructor: ->
		super

	fetchAllFlights : (callback) ->
		@retrieved = new Date()
		@flights = []
		@flights.push new Flight(
			"flightNumber": "IBE4785"
			"departure": "LEMD"
			"arrival": "LEBB"
			"route": "SIE UM601 BLV"
			"aircraft": "A320"
			"flightRules": "IFR"
			"clearedFix": ""
			"clearedFL": ""
			"finalFL": "280"
			"squawk" : "3674"
		)
		@flights.push new Flight(
			"flightNumber": "IBE1125"
			"departure": "LFPG"
			"arrival": "LEMD"
			"route": "IEA UM601 SIE"
			"aircraft": "A320"
			"flightRules": "IFR"
			"clearedFix": ""
			"clearedFL": ""
			"finalFL": "320"
			"squawk" : "0174"
		)
		@flights.push new Flight(
			"flightNumber": "RYR5387"
			"departure": "LEMD"
			"arrival": "LEXJ"
			"route": "SIE UM601 BLV"
			"aircraft": "A320"
			"flightRules": "IFR"
			"clearedFix": ""
			"clearedFL": ""
			"finalFL": "260"
			"squawk" : "3671"
		)
		do callback

window.FooFlightProvider = FooFlightProvider
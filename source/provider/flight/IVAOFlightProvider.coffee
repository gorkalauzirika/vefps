class IVAOFlightProvider extends FlightProvider

	constructor: ->
		super

	fetchAllFlights : (callback) ->
		@retrieved = new Date()
		@flights = []

		provider = @

		req = $.ajax 
			#url: 'http://eu20.ivan.ivao.aero/whazzup.txt'
			url: 'http://localhost:8080/flights/IVAO'
			datatype: 'jsonp'
			success: (data) ->
				provider.parseData data	
				do callback
			error: ->
				console.log 'Error'
		

	parseData: (data) ->
		start = new Date()
		lines = data.split("\n")
		for line in lines
			items = line.split(":")
			if items[3]? and items[3] is "PILOT"
				@flights.push new Flight(
					"flightNumber": items[0]
					"departure": items[11]
					"arrival": items[13]
					"route": items[30]
					"aircraft": items[9].split("/")[1]
					"flightRules": items[21]
					"finalFL": items[12]
					"squawk" : "XXXX"
				)
		finish = new Date()
		console.log finish - start 

window.IVAOFlightProvider = IVAOFlightProvider

class AsideView extends Backbone.View
	showOrHide: ->
		if @$el.is(":visible")
			do @hide
		else
			do @show

	show: ->
		do @$el.show
		@$el.css "left", do $(window).width - 300

	hide: ->
		@$el.css "left", do $(window).width
		view = @
		setTimeout ->
			do view.$el.hide
		, 500

window.AsideView = AsideView
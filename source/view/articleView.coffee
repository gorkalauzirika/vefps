class ArticleView extends Backbone.View
	el: $("article")

	events: ->

	initialize: ->
		@columns = {}

	addColumns: (columnNames) ->
		for col in columnNames
			@addColumn new FSColumn columnName: col

	addColumn: (column) ->
		if not @columns[column.get 'columnName']?
			col = new StripColumnView model: column
			@columns[column.get 'columnName'] = col
			@$el.append col.render().$el

	getColumn: (columnName) ->
		@columns[columnName]

window.ArticleView = ArticleView
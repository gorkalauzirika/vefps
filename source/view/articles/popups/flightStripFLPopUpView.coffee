class FlightStripFLPopUpView extends FlightStripPopUpView
	id: "fl-pop-up"

	events:->
		"keyup input#fl-filter": "filterByFL"
		"click li.flight-level": "levelSelected"

	initialize: ->
		super

		@$el.html "<div class='container'><ul></ul></div><input id='fl-filter' placeholder='New FL'/>"
		@$ul = @$el.find "ul"
		@$input = @$el.children "input"

		@flightLevels = []
		for i in [1..40]
			if i < 10 then @flightLevels.push "0#{i}0" else @flightLevels.push "#{i}0"
		@shownLevels = @flightLevels

	render: ->
		@$ul.html ""
		for level in @shownLevels
			@$ul.append "<li class='flight-level' data-flight-level='#{level}'>#{level}</li>"
		do @delegateEvents
		@

	show: ->
		super
		@shownLevels = @flightLevels
		do @render
		@$input.val ""
		do @$input.focus

	filterByFL:  (ev) ->
		if (ev.keyCode or ev.which) is 13 and @shownLevels.length > 0
			@model.set clearedFL: @shownLevels[0]
			do @hide
		else
			filter = ev.currentTarget.value
			@shownLevels = _.filter @flightLevels, (level) ->
				level.indexOf(filter) != -1
			do @render

	levelSelected: (ev) ->
		@model.set clearedFL: $(ev.target).data "flight-level"
		do @hide




window.FlightStripFLPopUpView = FlightStripFLPopUpView
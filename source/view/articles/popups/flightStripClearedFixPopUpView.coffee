class FlightStripClearedFixPopUpView extends FlightStripPopUpView
	id: "clr-pop-up"

	events:->
		"keyup input#fix-filter": "filterFixes"
		"click li.flight-level": "fixSelected"

	initialize: ->
		super

		@$el.html "<div class='container'><ul></ul></div><input id='fix-filter' placeholder='New Fix / Hdg'/>"
		@$ul = @$el.find "ul"
		@$input = @$el.children "input"

	render: ->
		@$ul.html ""
		###
		for level in @shownLevels
			@$ul.append "<li class='flight-level' data-flight-level='#{level}'>#{level}</li>"
		
		###
		do @delegateEvents
		@

	show: ->
		super

		do @render
		@$input.val ""
		do @$input.focus

	filterFixes:  (ev) ->
		if (ev.keyCode or ev.which) is 13
			@model.set clearedFix: @$input.val()
			do @hide
		###
		else
			filter = ev.currentTarget.value
			@shownLevels = _.filter @flightLevels, (level) ->
				level.indexOf(filter) != -1
			do @render
		###

	fixSelected: (ev) ->
		@model.set clearedFL: $(ev.target).data "fix"
		do @hide




window.FlightStripClearedFixPopUpView = FlightStripClearedFixPopUpView
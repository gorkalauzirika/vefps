class FlightStripPopUpView extends Backbone.View
	tagName: "div"
	className: "fs-pop-up"

	initialize: ->
		do @$el.hide

	locate: (posX, posY, width) ->
		@$el.css "left", posX - (@$el.width() - width) / 2
		@$el.css "top", posY - @$el.height()
		
	show: ->
		do @$el.show

	hide: ->
		do @$el.hide

	showOrHide: ->
		if @$el.is(":visible") then do @hide else do @show

window.FlightStripPopUpView = FlightStripPopUpView



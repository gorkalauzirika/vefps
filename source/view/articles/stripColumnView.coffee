class StripColumnView extends Backbone.View
	tagName: "div"
	className: "column"

	template: _.template '<h2><%= columnName %></h2><div class="strip-container"></div>'

	initialize: ->
		@flights = {}
		
		_.bindAll(this, "onSearchedFlightOver", "onSearchedFlightOut","onSearchedFlightDrop","onFlightStripReceive","onFlightStripRemove")


	render: ->
		@$el.html @template @model.toJSON()
		@$el.children(".strip-container").sortable
			connectWith: ".strip-container"
			placeholder: "strip-placeholder"
			forcePlaceholderSize: true
			out: @onFlightStripOut
			receive: @onFlightStripReceive
			remove: @onFlightStripRemove
		@$el.children(".strip-container").droppable
			accept: ".search-flight-item"
			over: @onSearchedFlightOver
			out: @onSearchedFlightOut
			drop: @onSearchedFlightDrop
		do @addSeparator
		@

	addFlightStrip: (flight) ->
		fsv = new FlightStripView model: flight
		@$el.children(".strip-container").append fsv.render().$el
		@flights[flight.get("flightNumber")] = flight

	addSeparator: ->
		for i in [1..4]
			@$el.children(".strip-container").append "<div class='strip-separator'>Separator #{i}</div>"

	onSearchedFlightOver: (ev,ui) ->
		if ui.position.left < 0
			@$el.css "background", "#AAB6CC"
		else
			@$el.css "background", "rgba(255, 255, 255, 0.8)"

	onSearchedFlightOut: ->
		@$el.css "background", "rgba(255, 255, 255, 0.8)"

	onSearchedFlightDrop: (ev,ui) ->
		@$el.css "background", "rgba(255, 255, 255, 0.8)"
		if ui.position.left < 0
			flight = VEFPS.flightProvider.searchByFlightNumber ui.draggable.attr "data-flight-number"
			if flight?
				@addFlightStrip flight

	onFlightStripReceive: (ev,ui) ->
		flNo = ui.item.attr("data-flight-number")
		@flights[flNo] = VEFPS.flightProvider.searchByFlightNumber flNo

	onFlightStripRemove: (ev,ui) ->
		delete @flights[ui.item.attr("data-flight-number")]

window.StripColumnView = StripColumnView
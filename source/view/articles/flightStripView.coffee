class FlightStripView extends Backbone.View
  tagName: "div"
  className: "flight-strip"

  template: _.template(
  	'<table>
  	 <tr>
  		<td class="flight-number" rowspan=2><ul><li class="small">CO.</li><li><%= flightNumber %></li><li class="small">Rules - Sq</li></ul></td>
  		<td class="aircraft"><ul><li><%= aircraft %></li><li class="small"><%= aircraftType %></li></ul></td>	
  		<td class="airports"><ul><li><%= arrival %></li><li class="small"><%= departure %></li></ul></td>
  		<td class="cleared-fix"><%= clearedFix %></td>
  		<td class="flight-level"><ul><li><%- clearedFL === "" ? "" : "FL" %><%= clearedFL %></li><li class="small"><%= finalFL %></li></ul></td>
  		<td class="transfer">x</td>
  	 <tr>
  	 	<td colspan=6><%= route %></td>
  	 </tr></table>') 

  events: ->
  	"click .cleared-fix" : "showClearedFixMenu"
  	"click .flight-level" : "showClearedFLMenu"
  	"click .transfer" : "showTransferMenu"

  initialize: ->
    @flightLevelView = new FlightStripFLPopUpView(model: @model)
    @clearedFixView = new FlightStripClearedFixPopUpView(model: @model)

    _.bindAll @, "render"
    @model.bind 'change', @render

  render: ->
    @$el.html @template @model.toJSON()
    @$el.append @flightLevelView.render().$el   
    @$el.append @clearedFixView.render().$el  
    flightNumber = @model.get("flightNumber")
    @$el.attr("data-flight-number", flightNumber) 
    @

  showClearedFixMenu: ->
    console.log "showClearedFixMenu"
    clf = @$el.find(".cleared-fix")
    @clearedFixView.locate clf.position().left, clf.position().top , clf.outerWidth()
    do @clearedFixView.showOrHide    

  showClearedFLMenu: ->
    console.log "showClearedFLMenu"
    flEl = @$el.find(".flight-level")
    @flightLevelView.locate flEl.position().left, flEl.position().top , flEl.outerWidth()
    do @flightLevelView.showOrHide    
    
  showTransferMenu: ->
  	console.log "showTransferMenu"

window.FlightStripView = FlightStripView
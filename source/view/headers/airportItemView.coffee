class AirportItemView extends Backbone.View
	tagName: "div"

	template: _.template "<div class='airport'><p class='airport-name'><%= airport %></p><p class='airport-winds'><%= wind %></p></div>" 

	events: ->
		"click div.airport" : "changeRunways"

	render: ->
		@$el.html @template @model

		if @model.departures isnt undefined 
			maxLength = if @model.departures.length > @model.arrivals.length then @model.departures.length else @model.arrivals.length
			for i in [0..maxLength - 1]
				if @model.departures[i] is @model.arrivals[i]
					@$el.append "<div class='runways'><p class='same'>#{@model.departures[i]}</p></div>"
				else
					dep = @model.departures[i] ? "&nbsp;"
					arr = @model.arrivals[i] ? "&nbsp;"
					@$el.append "<div class='runways'><p class='different'>#{dep}</p><p class='different'>#{arr}</p></div>"
		@

	changeRunways: ->
		alert 'runway modal'
		

window.AirportItemView = AirportItemView
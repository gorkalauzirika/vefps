class HeaderView extends Backbone.View
	el: $("header")

	events: ->
		"click span#departures" : "showChangeDeparturesMenu"
		"click span#arrivals" : "showChangeArrivalsMenu"
		"click span.weather" : "showWeatherMenu"
		"click span.settings" : "showSettingsMenu"
		"click span.search" : "showSearchMenu"

	initialize: ->

	updateInfo: ->
		$rwys = @$el.children('#active-rwy')
		
		rwys = VEFPS.settings.runways
		metars = VEFPS.weatherProvider.filterByAirports VEFPS.settings.airports 
		
		$rwys.html ''

		for i,airport of VEFPS.settings.airports
			departures = rwys[airport]?.departures
			arrivals = rwys[airport]?.arrivals
			wind = ""
			for metar in metars
				if do metar.getAirport is airport
					wind = do metar.getWind

			
			$arpt = new AirportItemView model:{airport: airport, departures: departures, arrivals: arrivals, wind: wind}

			$rwys.append $arpt.render().$el
			###
			$rwys.append "<div class='airport'><p class='airport-name'>#{airport}</p><p class='airport-winds'>#{wind}</p></div>"	
			if departures isnt undefined 
				maxLength = if departures.length > arrivals.length then departures.length else arrivals.length
				for i in [0..maxLength - 1]
					if departures[i] is arrivals[i]
						$rwys.append "<div class='runways'><p class='same'>#{departures[i]}</p></div>"
					else
						dep = departures[i] ? "&nbsp;"
						arr = arrivals[i] ? "&nbsp;"
						$rwys.append "<div class='runways'><p class='different'>#{dep}</p><p class='different'>#{arr}</p></div>"
			###

	showChangeDeparturesMenu: ->
		console.log 'show departures menu'
		do @updateInfo

	showChangeArrivalsMenu: ->

	showWeatherMenu: ->
		@hideAllAsidesInstead "weather"
		@$el.children('#right-toolbar').children('.weather').toggleClass("selected")
		do VEFPS.views.weatherView.showOrHide

			
	showSettingsMenu: ->
		@hideAllAsidesInstead "settings"
		@$el.children('#right-toolbar').children('.settings').toggleClass("selected")
		do VEFPS.views.settingsView.showOrHide

	showSearchMenu: ->
		@hideAllAsidesInstead "search"
		@$el.children('#right-toolbar').children('.search').toggleClass("selected")
		do VEFPS.views.searchView.showOrHide

	hideAllAsidesInstead: (dontHide) ->
		if dontHide isnt "weather"
			do VEFPS.views.weatherView.hide
			@$el.children('#right-toolbar').children('.weather').removeClass("selected")
		if dontHide isnt "search"
			do VEFPS.views.searchView.hide			
			@$el.children('#right-toolbar').children('.search').removeClass("selected")
		if dontHide isnt "settings"
			do VEFPS.views.settingsView.hide
			@$el.children('#right-toolbar').children('.settings').removeClass("selected")
		

window.HeaderView = HeaderView
class SearchFlightItem extends Backbone.View
  tagName: "li"
  className: "search-flight-item"

  template: _.template "<span><%= flightNumber %> <%= departure %> - <%= arrival %></span>" 

  render: ->
  	@$el.html @template @model.toJSON()
  	@$el.attr "data-flight-number", @model.attributes.flightNumber
  	@$el.draggable
  		revert: true
  		revertDuration: 50
  	@

window.SearchFlightItem = SearchFlightItem
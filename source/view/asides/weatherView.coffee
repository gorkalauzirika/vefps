class WeatherView extends AsideView
	el: $("aside#weather")

	initialize: ->
		@$el.css "left", do $(window).width
		@$metars = @$el.children "#metars"
		do @render

	render: ->
		@$metars.html ""
		weather = VEFPS.weatherProvider.filterByAirports VEFPS.settings.airports
		for w in weather
			item = new WeatherItemView model: w
			@$metars.append item.render().el

window.WeatherView = WeatherView
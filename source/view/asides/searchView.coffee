class SearchView extends AsideView
	el: $("aside#search")

	events: ->
		"keyup input#flight-search" : "searchUpdated"
		
	initialize: ->
		@inbound = []
		@outbound = []
		@inboundList = @$el.children("#inbound")
		@outboundList = @$el.children("#outbound")
		@foundList = @$el.children("#found")
		@foundHeader = @$el.children("#found-header")
		@$el.css "left", do $(window).width
		do @loadFlights
		do @render

	loadFlights: ->
		flights = VEFPS.flightProvider.filterByAirports VEFPS.settings.airports
		@inbound = flights.inbound
		@outbound = flights.outbound

	render: ->
		for inb in @inbound
			it = new SearchFlightItem model: inb
			@inboundList.append it.render().el

		for out in @outbound
			it =  new SearchFlightItem model: out
			@outboundList.append it.render().el

		@$el.children(".strip-container").droppable
			accept: ".search-flight-item"

	searchUpdated: (ev,ui)->
		flights = VEFPS.flightProvider.filterByFlightNumber ev.currentTarget.value
		@foundList.html ""
		maxResults = 10
		results = 0
		if ev.currentTarget.value is "" 
			do @foundHeader.hide
		else
			do @foundHeader.show
			if flights.length is 0 
				@foundList.append "<li>No results found</li>"
			else
				for flight in flights
					f = new SearchFlightItem model: flight
					@foundList.append f.render().el
					results++
					if results is maxResults then return

window.SearchView = SearchView
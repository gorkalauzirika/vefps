class WeatherItemView extends Backbone.View
  tagName: "li"
  className: "weather-item"

  template: _.template "<%= rawMetar %>" 

  render: ->
    @$el.html @template @model.toJSON()
    @

window.WeatherItemView = WeatherItemView
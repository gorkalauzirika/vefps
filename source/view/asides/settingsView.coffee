class SettingsView extends AsideView
	el: $("aside#settings")

	initialize: ->
		@$el.css "left", do $(window).width
		@airportsList = @$el.children "#airports"
		@columnList = @$el.children "#columns"
		@airportInput = @$el.children "#add-airport"
		@columnInput = @$el.children "#add-column"
		@airports = VEFPS.settings.airports
		@columns = VEFPS.settings.columns
		for airport in @airports
			@airportsList.append "<li>#{airport}</li>"
		for column in @columns
			@columnList.append "<li>#{column}</li>"

	events: ->
		"keyup input#add-column" : "addColumn"
		"keyup input#add-airport" : "addAirport"
		"click button#save-settings" : "saveSettings"

	addColumn: (ev) ->
		if (ev.keyCode or ev.which) is 13
			@columnList.append "<li>#{ev.currentTarget.value}</li>"
			@columns.push ev.currentTarget.value
			@columnInput.val ""

	addAirport: (ev) ->
		if (ev.keyCode or ev.which) is 13
			@airportsList.append "<li>#{ev.currentTarget.value}</li>"
			@airports.push ev.currentTarget.value
			@airportInput.val ""			

	saveSettings: ->
		VEFPS.settings.airports = @airports
		do VEFPS.views.headerView.updateInfo

		VEFPS.settings.columns = @columns
		VEFPS.views.articleView.addColumns @columns

window.SettingsView = SettingsView
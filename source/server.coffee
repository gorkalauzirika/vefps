express = require 'express'
http = require 'http'
fs = require 'fs'
app = do express
server = app.listen(8080)


app.use("/css", express.static(__dirname + '/css'))
app.use("/js", express.static(__dirname + '/js'));
app.use("/images", express.static(__dirname + '/images'));

app.get '/', (req, res) -> 
  res.sendfile(__dirname + '/debug.html') 
  console.log 'Index page sent'

app.get '/flights/IVAO', (req, res) -> 
  res.charset = 'utf-8';
  res.sendfile __dirname + '/documents/flights/IVAO.txt'

app.get '/weather/IVAO', (req, res) -> 
  res.charset = 'utf-8';
  res.sendfile __dirname + '/documents/weather/IVAO.txt'

setInterval ->
	IVAOFlights = fs.createWriteStream __dirname + "/documents/flights/IVAO-temp.txt"
	http.get "http://eu20.ivan.ivao.aero/whazzup.txt", (response) ->
		pipe = response.pipe IVAOFlights
		pipe.on 'close', -> 
			fs.createReadStream(__dirname + '/documents/flights/IVAO-temp.txt').pipe fs.createWriteStream __dirname + '/documents/flights/IVAO.txt'
			console.log "IVAO flights downloaded"

	IVAOWeather = fs.createWriteStream __dirname + '/documents/weather/IVAO-temp.txt'
	http.get "http://wx.ivao.aero/metar.php", (response) ->
		pipe = response.pipe IVAOWeather
		pipe.on 'close', -> 
			fs.createReadStream(__dirname + '/documents/flights/IVAO-temp.txt').pipe fs.createWriteStream __dirname + '/documents/weather/IVAO.txt'
			console.log "IVAO weather downloaded" 
,60000



console.log 'Server started' 

class Flight extends Backbone.Model

	defaults: ->
		"flightNumber" : null
		"departure": ""
		"arrival": ""
		"route": ""
		"aircraft": ""
		"aircraftType" : ""
		"flightRules": ""
		"clearedFix": ""
		"clearedFL": ""
		"finalFL": ""
		"squawk" : ""

window.Flight = Flight

$(document).ready ->
	VEFPS.settings = 
		columns: ["Departures","Arrivals","Local"]
		airports: ["LEMD","LEBB","LEXJ","LESO"]
		runways:
			LEMD:
				departures: ["36L","36R","32R"]
				arrivals: ["32L", "32R"]
			LEBB:
				departures: ["30"]
				arrivals: ["30"]
			LEXJ:
				departures: ["29"]
				arrivals: ["29"]
			LESO:
				departures: ["04"]
				arrivals: ["04"]

	VEFPS.flightProvider = new IVAOFlightProvider()
	VEFPS.weatherProvider = new IVAOWeatherProvider()
	VEFPS.airportProvider = new FooAirportProvider()

	VEFPS.views = {}
	VEFPS.views.headerView = new HeaderView()
	
	VEFPS.views.articleView = new ArticleView()
	VEFPS.views.settingsView = new SettingsView()
	
	

	VEFPS.flightProvider.fetchAllFlights ->
		VEFPS.views.searchView = new SearchView
		VEFPS.views.articleView.addColumns VEFPS.settings.columns

	VEFPS.airportProvider.getAirportsInfo VEFPS.settings.airports, ->
	
	VEFPS.weatherProvider.fetchAllWeather ->
		VEFPS.views.weatherView = new WeatherView
		do VEFPS.views.headerView.updateInfo

window.VEFPS = {}




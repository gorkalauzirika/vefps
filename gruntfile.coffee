module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON "package.json"

    meta:
      endpoint: ''
      banner: """
        /* <%= pkg.name %> v<%= pkg.version %> - <%= grunt.template.today("m/d/yyyy") %>
           <%= pkg.homepage %>
           Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %> - Licensed <%= _.pluck(pkg.license, "type").join(", ") %> */

        """

    source:
      coffee: 
        index:
          [ 'source/model/*.coffee',
            'source/provider/*.coffee'
            'source/provider/flight/*.coffee'
            'source/provider/weather/*.coffee'
            'source/provider/airport/*.coffee'
            'source/view/*.coffee',
            'source/view/articles/*.coffee',
            'source/view/articles/popups/*.coffee',
            'source/view/asides/*.coffee',
            'source/view/headers/*.coffee',
            'source/index.coffee']

        server:
          [ 'source/server.coffee']


      stylesheets: 'source/stylesheets/'

      jade: 'source/jade/'

    coffee:
      compile: 
        files: 
          '<%= meta.endpoint %>js/index.debug.js': ['<%= source.coffee.index %>']
          '<%= meta.endpoint %>server.debug.js': ['<%= source.coffee.server %>']

    jade:
      debug:
        options:
          data:
            debug: true
            timestamp: "<%= new Date().getTime() %>"
        files:
          'debug.html': '<%= source.jade %>index.jade'

    uglify:
      options: compress: false, banner: "<%= meta.banner %>"
      coffee: 
        files: 
          '<%= meta.endpoint %>js/index.js': '<%= meta.endpoint %>js/index.debug.js'
          '<%= meta.endpoint %>server.js': '<%= meta.endpoint %>js/server.debug.js'

    compass:
      dist: 
        options: 
          sassDir: "<%= source.stylesheets %>"
          cssDir:'<%= meta.endpoint %>css'     

    watch:
      coffee:
        files: ["<%= source.coffee.index %>", "<%= source.coffee.server %>" ]
        tasks: ["coffee"]
      stylesheets:
        files: ["<%= source.stylesheets %>*.scss"]
        tasks: ["compass"]
      jade:
        files: ["<%= source.jade %>*.jade"]
        tasks: ["jade"]


  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-compass"
  #grunt.loadNpmTasks "grunt-contrib-concat"
  grunt.loadNpmTasks 'grunt-contrib-jade'
  grunt.loadNpmTasks "grunt-contrib-watch"


  grunt.registerTask "default", ["coffee", "uglify", "compass","jade"] #"concat"]

  ###
    concat:
      css:
        src: ['<%= source.css_core %>'],
        dest: '<%= meta.endpoint %>static/stylesheets/watch.css'
###
